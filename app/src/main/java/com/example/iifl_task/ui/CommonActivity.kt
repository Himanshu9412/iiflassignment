package com.example.iifl_task.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.iifl_task.R

class CommonActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_common)
    }
}