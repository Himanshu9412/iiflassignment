package com.example.iifl_task.core

sealed class Status {
    object LOADING : Status()
    object SUCCESS : Status()
    object ERROR : Status()
}

data class UiModel<out T>(val status: Status, val data: T?, val message: String?) {

    companion object {

        fun <T> success(data: T?): UiModel<T> {
            return UiModel(Status.SUCCESS, data, null)
        }

        fun <T> error(msg: String, data: T?): UiModel<T> {
            return UiModel(Status.ERROR, data, msg)
        }

        fun <T> loading(): UiModel<T> {
            return UiModel(Status.LOADING, null, null)
        }

    }

}
