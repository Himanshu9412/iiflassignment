package com.example.iifl_task.core

import android.content.Context
import android.widget.ProgressBar
import android.widget.Toast
import androidx.core.view.isVisible

object UiHelper {
    fun ProgressBar.toggleProgressBar(status: Status) {
        isVisible = status is Status.LOADING
    }

    fun String.showToast(context: Context){
        Toast.makeText(context, this, Toast.LENGTH_SHORT).show()
    }
}